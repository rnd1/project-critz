## cara install

```
$ cd C:xampp/htdocs
$ git clone https://gitlab.com/rnd1/project-critz.git
$ cd project-critz
$ composer install
```
- jangan lupa bikin .env, copy dari .env.example terus edit sesuai kebutuhan

### cara tarik data terbaru
```
$ git pull origin master
```

### cara update commit

- edit datanya
- buka git bash
- `$ git add .`
- `$ git commit -m 'tulis pesannya apa disini yaaaaaa'``

### cara push update ke server
```
$ git push origin master
```